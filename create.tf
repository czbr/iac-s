provider "aws" {
    region = "us-east-2"
}

resource "aws_instance" "first_web_server"{
    # ami                     ="ami-00399ec92321828f5"
    ami                     =  var.ami
    # instance_type           ="t2.micro"
    instance_type   =  var.instance_type

    tags                    = {
        Name = "Terraform-first-server"
    }
    user_data               = <<-EOF
                                #!/bin/bash
                                echo "Hello World" > index.html
                                nohup busybox httpd -f -p 8080 &
                                EOF
    vpc_security_group_ids  = [aws_security_group.sec_fws.id]
}

resource "aws_security_group" "sec_fws" {
    name = "Terraform-first-server-security"
    ingress {
       from_port    = 8080
       to_port      = 8080
       protocol     = "tcp"
       cidr_blocks  = ["0.0.0.0/0"]
    }
}
